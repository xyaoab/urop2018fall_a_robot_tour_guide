# UROP_Wifi_Localization-
This is mainly for UROP of the robot tour guide. 

2017: 
- Using the nearest search method to find the predicted position and test finishing.
- Tried GP from high dimensional to low dimensional mapping 

2018: 
- GP from low dimension to high dimension on grid 
- Bayes filter to localize on map grid
